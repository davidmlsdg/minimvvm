﻿using System;
using System.Windows.Input;

namespace MiniMvvm.Commands
{
    public class RelayCommand : ICommand
    {
        #region Private Campos

        private Func<bool> targetCanExecuteMethod;
        private Action targetExecuteMethod;

        #endregion Private Campos

        #region Public Construtoras

        public RelayCommand(Action targetExecuteMethod)
        {
            this.targetExecuteMethod = targetExecuteMethod;
        }

        public RelayCommand(Action targetExecuteMethod, Func<bool> targetCanExecuteMethod) : this(targetExecuteMethod)
        {
            this.targetCanExecuteMethod = targetCanExecuteMethod;
        }

        #endregion Public Construtoras

        #region Public Eventos

        public event EventHandler CanExecuteChanged = delegate { };

        #endregion Public Eventos

        #region Public Métodos

        public bool CanExecute(object parameter)
        {
            if (targetCanExecuteMethod != null)
                return targetCanExecuteMethod();
            if (targetExecuteMethod != null)
                return true;
            return false;
        }

        public void Execute(object parameter)
        {
            if (targetExecuteMethod != null)
                targetExecuteMethod();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }

        #endregion Public Métodos
    }

    public class RelayCommand<T> : ICommand
    {
        #region Private Campos

        private Action onIncluirFabricanteCommand;
        private Func<T, bool> targetCanExecuteMethod;
        private Action<T> targetExecuteMethod;

        #endregion Private Campos

        #region Public Construtoras

        public RelayCommand(Action<T> targetExecuteMethod)
        {
            this.targetExecuteMethod = targetExecuteMethod;
        }

        public RelayCommand(Action onIncluirFabricanteCommand)
        {
            this.onIncluirFabricanteCommand = onIncluirFabricanteCommand;
        }

        public RelayCommand(Action<T> targetExecuteMethod, Func<T, bool> targetCanExecuteMethod) : this(targetExecuteMethod)
        {
            this.targetCanExecuteMethod = targetCanExecuteMethod;
        }

        #endregion Public Construtoras

        #region Public Eventos

        public event EventHandler CanExecuteChanged = delegate { };

        #endregion Public Eventos

        #region Public Métodos

        public bool CanExecute(object parameter)
        {
            if (targetCanExecuteMethod != null)
            {
                T param = (T)parameter;
                return targetCanExecuteMethod(param);
            }
            if (targetExecuteMethod != null)
                return true;
            return false;
        }

        public void Execute(object parameter)
        {
            if (targetExecuteMethod != null)
                targetExecuteMethod((T)parameter)
                    ;
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }

        #endregion Public Métodos
    }
}
