﻿namespace MiniMvvm.Services
{
    // Resumo: Especifica os botões exibidos em uma caixa de mensagem. Usado como um argumento do
    // método Overload:System.Windows.MessageBox.Show.
    public enum MessageDialogButton
    {
        // Resumo: A caixa de mensagem exibe um botão OK.
        OK = 0,

        // Resumo: A caixa de mensagem exibe os botões OK e Cancelar.
        OKCancel = 1,

        // Resumo: A caixa de mensagem exibe os botões Sim, Não e Cancelar.
        YesNoCancel = 3,

        // Resumo: A caixa de mensagem exibe os botões Sim e Não.
        YesNo = 4
    }
}