﻿using System.Windows;

namespace MiniMvvm.Services
{
    /// <summary>
    /// Exibe uma mensagem ao usuário
    /// </summary>
    public class MessageDialogService : IMessageDialogService
    {
        #region Public Métodos

        public MessageDialogResult Show(string messageText)
        {
            MessageBox.Show(messageText);
            return MessageDialogResult.OK;
        }

        public MessageDialogResult Show(string messageText, string caption, MessageDialogButton button)
        {
            return MessageBoxToDialogResult(MessageBox.Show(messageText, caption, DialogToMessageBoxButton(button)));
        }

        #endregion Public Métodos

        #region Private Métodos

        private static MessageBoxButton DialogToMessageBoxButton(MessageDialogButton dialogButton)
        {
            return (MessageBoxButton)dialogButton;
        }

        private static MessageDialogResult MessageBoxToDialogResult(MessageBoxResult messageBoxResult)
        {
            return (MessageDialogResult)messageBoxResult;
        }

        #endregion Private Métodos

        public static MessageDialogResult ShowDialog(string messageText)
        {
            MessageBox.Show(messageText);
            return MessageDialogResult.OK;
        }

        public static MessageDialogResult ShowDialog(string messageText, string caption, MessageDialogButton button)
        {
            return MessageBoxToDialogResult(MessageBox.Show(messageText, caption, DialogToMessageBoxButton(button)));
        }
    }
}