﻿namespace MiniMvvm.Services
{
    // Resumo: Especifica em qual botão da caixa de mensagem o usuário clica.
    // System.Windows.MessageBoxResult é retornado pelo método Overload:System.Windows.MessageBox.Show.
    public enum MessageDialogResult
    {
        // Resumo: A caixa de mensagem não retornará resultados.
        None = 0,

        // Resumo: O valor do resultado da caixa de mensagem é OK.
        OK = 1,

        // Resumo: O valor do resultado da caixa de mensagem é Cancelar.
        Cancel = 2,

        // Resumo: O valor do resultado da caixa de mensagem é Sim.
        Yes = 6,

        // Resumo: O valor do resultado da caixa de mensagem é Não.
        No = 7
    }
}