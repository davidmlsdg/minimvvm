﻿namespace MiniMvvm.Services
{
    public interface IMessageDialogService
    {
        #region Public Métodos

        MessageDialogResult Show(string messageText);

        MessageDialogResult Show(string messageText, string caption, MessageDialogButton button);

        #endregion Public Métodos
    }
}