﻿using System.Windows;
using System.Windows.Input;

namespace MiniMvvm.Services
{
    public interface IUserControlService
    {
        /// <summary>
        /// 
        /// </summary>
        Cursor Cursor { get; set; }

        /// <summary>
        /// 
        /// </summary>
        Visibility Visibility { get; set; }
    }
}
