using MiniMvvm.Events;

using System;

using Xunit;

namespace MiniMvvmTestes.Events
{

    public class DelegateReferenceFixture
    {
        [Fact]
        public void KeepAlivePreventsDelegateFromBeingCollected()
        {
            var delegates = new SomeClassHandler();
            var delegateReference = new DelegateReference(
                (Action<string>)delegates.DoEvent, true);

            delegates = null;
            GC.Collect();

            Assert.NotNull(delegateReference.Target);
        }

        [Fact]
        public void NotKeepAliveAllowsDelegateToBeCollected()
        {
            var delegates = new SomeClassHandler();
            var delegateReference = new DelegateReference(
                (Action<string>)delegates.DoEvent, false);

            delegates = null;
            GC.Collect();

            Assert.Null(delegateReference.Target);
        }

        [Fact]
        public void NotKeepAliveKeepsDelegateIfStillAlive()
        {
            var delegates = new SomeClassHandler();
            var delegateReference = new DelegateReference(
                (Action<string>)delegates.DoEvent, false);

            GC.Collect();

            Assert.NotNull(delegateReference.Target);

            // Makes delegates ineligible for garbage collection until this point 
            // (to prevent oompiler optimizations that may release the referenced
            // object prematurely).
            GC.KeepAlive(delegates);
            delegates = null;
            GC.Collect();

            Assert.Null(delegateReference.Target);
        }

        [Fact]
        public void TargetShouldReturnAction()
        {
            var classHandler = new SomeClassHandler();
            Action<string> myAction = classHandler.MyAction;

            var weakAction = new DelegateReference(myAction, false);

            ((Action<string>)weakAction.Target)("payload");
            Assert.Equal("payload", classHandler.MyActionArg);
        }

        [Fact]
        public void ShouldAllowCollectionOfOriginalDelegate()
        {
            var classHandler = new SomeClassHandler();
            Action<string> myAction = classHandler.MyAction;

            var weakAction = new DelegateReference(myAction, false);

            var originalAction = new WeakReference(myAction);
            myAction = null;
            GC.Collect();
            Assert.False(originalAction.IsAlive);

            ((Action<string>)weakAction.Target)("payload");
            Assert.Equal("payload", classHandler.MyActionArg);
        }

        [Fact]
        public void ShouldReturnNullIfTargetNotAlive()
        {
            var handler = new SomeClassHandler();
            var weakHandlerRef = new WeakReference(handler);

            var action = new DelegateReference(
                (Action<string>)handler.DoEvent, false);

            handler = null;
            GC.Collect();
            Assert.False(weakHandlerRef.IsAlive);

            Assert.Null(action.Target);
        }

        [Fact]
        public void WeakDelegateWorksWithStaticMethodDelegates()
        {
            var action = new DelegateReference(
                (Action)SomeClassHandler.StaticMethod, false);

            Assert.NotNull(action.Target);
        }

        public class SomeClassHandler
        {
            public string MyActionArg;

            public void DoEvent(string value)
            {
                var myValue = value;
            }

            public static void StaticMethod()
            {
                int i = 0;
            }

            public void MyAction(string arg)
            {
                this.MyActionArg = arg;
            }
        }
    }
}
