using MiniMvvm.Events;

using System;

namespace MiniMvvmTestes.Events
{

    public class MockDelegateReference : IDelegateReference
    {
        public Delegate Target { get; set; }

        public MockDelegateReference()
        {
        }

        public MockDelegateReference(Delegate target)
        {
            this.Target = target;
        }
    }
}